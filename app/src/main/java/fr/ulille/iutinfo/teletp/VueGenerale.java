package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    private String salle;
    private String poste;
    private String DISTANCIEL = "0";
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        /*this.DISTANCIEL = getActivity().getResources().getString(R.string-array.list_salles);*/
        this.poste = "0";
        this.salle = DISTANCIEL;
        // TODO Q4

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {

            /* model.setUsername(findViewById(R.id.tvLogin)).getText()); */

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        getActivity().findViewById(R.id.spPoste).setOnItemSelectedListener(v -> {
            this.update();
        });

        getActivity().findViewById(R.id.spSalle).setOnItemSelectedListener(v -> {
            this.update();
        });
        // TODO Q9
    }


    public void update() {
        if (this.salle.equals("Distanciel")) {
            ((View) getActivity().findViewById(R.id.spPoste)).setVisibility(View.GONE);
            ((View) getActivity().findViewById(R.id.spPoste)).setEnabled(false);
            model.setLocalisation("Distanciel");
        } else {
            ((View) getActivity().findViewById(R.id.spPoste)).setVisibility(View.VISIBLE);
            ((View) getActivity().findViewById(R.id.spPoste)).setEnabled(true);
            model.setLocalisation(this.salle + this.poste);
        }
    }
    // TODO Q9
}